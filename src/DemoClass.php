<?php

namespace Demo\Package;

use Carbon\Carbon;

class DemoClass extends Carbon
{
	public function say($message="Hello World!")
	{
		return $message;
	}
}
